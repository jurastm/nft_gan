import numpy as np

def convert_torch_images_to_numpy(batch):
    images = batch.detach().cpu().numpy()
    images = images.transpose((0, 2, 3, 1))
    images = (images * 0.5) + 0.5
    images = images * 255
    return np.clip(images, 0, 255).astype(np.uint8)
