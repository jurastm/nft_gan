import argparse
import io
from PIL import Image
import cv2
import numpy as np
import torch
import torch.nn as nn
import torchvision.transforms as T
import torch.nn.functional as F
import torchvision.models as models
import torchvision.utils as vutils

from gan.gan import Generator256 as Generator
from utils import convert_torch_images_to_numpy
from flask import Flask, render_template, request, redirect

app = Flask(__name__)

@app.route("/", methods=["GET", "POST"])
def generate_image():
    
    if request.method == 'POST':
        if 'file' not in request.files:
            print('file no in request')
            return redirect(request.url)
        
        file_a = request.files['file']
        if not file_a:
            return

        img_bytes = file_a.read()
        img = Image.open(io.BytesIO(img_bytes))
        img_numpy = np.array(img)
        #img_numpy = cv2.cvtColor(img_numpy, cv2.COLOR_BGR2RGB)
        #img = Image.fromarray(img_numpy)

        img_batch = data_transform(img).unsqueeze(0)
        with torch.no_grad():
            fixed_features = feature_extractor_network(img_batch.to(device))
            fixed_features = fixed_features.detach().cpu()
        
        fixed_noise = torch.randn(1, noise_dem)
        
        generator_network.eval()
        with torch.no_grad():
            fake_images = generator_network(fixed_noise.to(device), fixed_features.to(device))
        
        fake_images_numpy = convert_torch_images_to_numpy(fake_images)
        image_to_save = cv2.cvtColor(fake_images_numpy[0], cv2.COLOR_BGR2RGB)

        # Save to static to visualize.
        cv2.imwrite('static/result0.jpg', image_to_save)
        # Redirect to drawn image 
        return redirect('static/result0.jpg')

    return render_template('index.html')


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description="Flask app exposing Face Detection and face alignment module")
    parser.add_argument("--port", default=5000, type=int, help="port number")
    args = parser.parse_args()

    fe_weights = 'gan/weights/model.pt'
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    #Create and load the metric feature extractor network
    feature_extractor_network = models.resnet18(pretrained=False)
    modules = list(feature_extractor_network.children())[:-1] #Remove fully connected layer
    modules.append(nn.Flatten())
    feature_extractor_network = nn.Sequential(*modules)
    feature_extractor_network = feature_extractor_network.to(device)
    feature_extractor_network.load_state_dict(torch.load(fe_weights, map_location=device))
    feature_extractor_network.eval()

    image_size = 256
    noise_dem = 128
    feature_dem = 512
    ch_multi = 32
    norm_type = 'bn'

    generator_network = Generator(in_noise=noise_dem,
                              in_features=feature_dem,
                              ch=ch_multi,
                              norm_type=norm_type).to(device)

    g_state_dict = torch.load('gan/weights/gan_bs48.pt', map_location=device)['G_state_dict']
    generator_network.load_state_dict(g_state_dict)

    data_transform  = T.Compose([
    T.Resize(image_size),
    T.CenterCrop(image_size),
    T.RandomHorizontalFlip(p=0.5),
    T.ToTensor(),
    T.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
    ])

    app.run(host="0.0.0.0", port=args.port)







        