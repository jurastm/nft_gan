# Open Gan inference model with web app
Simple web app with Flask backend that generates flower's images on based on given image. <br />
How it works? <br />
1. Feed into the model any image of any flower.
2. Pre-trained feature extractor model extracts embedding vector.
3. Embedding and randomly generated noise vector are fed into Generator model.
4. Given this data Generator creates image

## Before you start
Download pretrained weights and put them to `gan/weights/` folder <br />
Feature extractor weights: <br />
https://drive.google.com/file/d/1COiK2QMSjW-66UW6MGm8EyJDEQMY8mwI/view?usp=sharing
Generator weights: <br />
https://drive.google.com/file/d/1Wg8AZE3-nj3YL9yR07Q3Kp1lSlCmuRCU/view?usp=sharing

## Web app
Simple app consisting of a form where you can upload an image, and see the inference result of the model in the browser. <br /> Run: `$ python app.py --port 5001`

then visit http://localhost:5001/ in your browser:

![Generated image](static/result0.jpg?raw=true "Generated image.")
